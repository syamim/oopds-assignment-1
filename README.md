## Data structure in users.csv
# recipient
name,phone,status,vaccine center name @ vaccined date @ vaccine type(optional),vaccine center name @ vaccined date @ vaccine type(optional),email,password,user type
# vaccine center
name,phone,empty,empty,empty,email,password,user type
# ministry of health
name,phone,empty,empty,empty,email,password,user type


## Folder Structure

The workspace contains two folders by default, where:

- `src`: the folder to maintain sources
- `lib`: the folder to maintain dependencies

Meanwhile, the compiled output files will be generated in the `bin` folder by default.

> If you want to customize the folder structure, open `.vscode/settings.json` and update the related settings there.


## Dependency Management

The `JAVA PROJECTS` view allows you to manage your dependencies. More details can be found [here](https://github.com/microsoft/vscode-java-dependency#manage-dependencies).
