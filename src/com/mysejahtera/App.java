package com.mysejahtera;

import java.io.*; 
import java.util.*;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;

public class App {
  static String path = "";
  public static void main(String[] args) throws Exception, IOException{
    readCsv(); // init reading csv file for database
  }

  public static void readCsv() throws IOException, CsvException{
    List<Recipient> recipientsList = new ArrayList<>();
    List<VaccineCenter> vacList = new ArrayList<>();
    List<MinHealth> mohList = new ArrayList<>();
    var fileName = App.path + "users.csv";
    try (CSVReader reader = new CSVReaderBuilder(new FileReader(fileName))
      .withSkipLines(1) //skip header
      .build()
    ){
      List<String[]> r = reader.readAll();
      r.forEach(userData -> {
        if (userData[7].equals("recipient")){
          Recipient tempRec = new Recipient(userData);
          recipientsList.add(tempRec);
        }else if (userData[7].equals("vac")){
          VaccineCenter tempVac = new VaccineCenter(userData);
          vacList.add(tempVac);
        }else{ // moh catcher
          MinHealth tempMoh = new MinHealth(userData);
          mohList.add(tempMoh);
        }
      });
    }

    User users = new User(recipientsList, vacList, mohList);
    App.mainMenu(users);
  }

  public static void loginLoop(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    var wrapper = new Object(){ 
      Boolean IsVerified = false;
      Boolean userFound = false;
      Boolean errorExist = false;
    };
    while(!wrapper.IsVerified){
      display.clearConsole();
      display.println("Please login to continue", "success");
      display.print("Enter email : ", "input");
      String email = sc.nextLine();
      display.print("Enter password : ", "input");
      String password = sc.nextLine();
      users.recipientsList.forEach(user ->{
        Map<String, String> temp = user.getCredential();
        if (temp.get("email").equals(email)){
          wrapper.userFound = true;
          if (temp.get("password").equals(password)){
            wrapper.IsVerified = true;
            displayRecipientMenu(user, users);
          }else{
            wrapper.errorExist = true;
          }
        }else{
          wrapper.errorExist = true;
        }
      });

      if (!wrapper.userFound){
        users.vacList.forEach(user ->{
          Map<String, String> temp = user.getCredential();
          if (temp.get("email").equals(email)){
            wrapper.userFound = true;
            if (temp.get("password").equals(password)){
              wrapper.IsVerified = true;
              displayVacMenu(user, users);
            }else{
              wrapper.errorExist = true;
            }
          }else{
            wrapper.errorExist = true;
          }
          return;
        });
      }
      
      if (!wrapper.userFound){
        users.mohList.forEach(user ->{
          Map<String, String> temp = user.getCredential();
          if (temp.get("email").equals(email)){
            wrapper.userFound = true;
            if (temp.get("password").equals(password)){
              wrapper.IsVerified = true;
              displayMohMenu(user, users);
            }else{
              wrapper.errorExist = true;
            }
          }else{
            wrapper.errorExist = true;
          }
          return;
        });
      }
      
      if (wrapper.errorExist && wrapper.IsVerified == false){
        display.clearConsole();
        display.println("Email and password entered is incorrect", "error");
      }
    }
  }
  
  public static void displayRecipientMenu(Recipient user, User users){
    TerminalColor display = new TerminalColor();
    var wrapper = new Object(){
      Boolean endProgram = false;
    };
    while(!wrapper.endProgram){
      display.println("Welcome " + user.name, "success");
      String first_appointment, first_type;
      if (user.first_dose.get("isEmpty").equals("false")){
        first_appointment = user.first_dose.get("date") + " at " + user.first_dose.get("center");
        first_type = user.first_dose.get("type");
      }else{
        first_appointment = "Date not yet assigned";
        first_type = "Not yet assigned";
      }

      String second_appointment, second_type;
      if (user.second_dose.get("isEmpty").equals("false")){
        second_appointment = user.second_dose.get("date") + " at " + user.second_dose.get("center");
        second_type = user.second_dose.get("type");
      }else{
        second_appointment = "Date not yet assigned";
        second_type = "Not yet assigned";
      }
      display.println("Status : " + user.status, "basic");
      display.println("First Dose", "success");
      display.println("Appointment : " + first_appointment, "warning");
      display.println("Vaccine Type : " + first_type, "warning");
      display.println("Second Dose", "success");
      display.println("Appointment : " + second_appointment, "warning");
      display.println("Vaccine Type : " + second_type, "warning");
      Scanner sc = new Scanner(System.in);
      display.print("Press Enter to Logout....", "success");
      sc.nextLine();
      wrapper.endProgram = true;
    }
    display.clearConsole();
    App.mainMenu(users);
  }

  public static void displayMohMenu(MinHealth user, User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    MinHealth moh = new MinHealth();
    var wrapper = new Object(){
      Boolean loggedIn = true;
    };
    display.clearConsole();
    while (wrapper.loggedIn) {
      display.println("Welcome " + user.name, "success");
      display.println("Below is what you can do : ", "input");
      display.println("1. View all recipient", "basic");
      display.println("2. Search for recipient", "basic");
      display.println("3. Supply vaccine to Vaccine Center", "basic");
      display.println("4. Assign recipient to Vaccine Center", "basic");
      display.println("5. View statistic of all Vaccine Center", "basic");
      display.println("6. Logout", "basic");
      display.print("Enter you option : ", "input");
      try {
        int menu = Integer.parseInt(sc.nextLine()); 
        switch (menu) {
          case 1:
            moh.viewRecipient(users);
            break;
          case 2:
            moh.searchRecipient(users);
            break;
          case 3:
            users = moh.supplyVaccine(users);
            break;
          case 4:
            users = moh.assignRecipient(users);
            break;
          case 5:
            moh.analytics(users);
          case 6:
            wrapper.loggedIn = false;
            break;
        }
      } catch (NumberFormatException e) {
        display.wrongMenu();
      }
    }
    App.mainMenu(users);
  }
  
  public static void displayVacMenu(VaccineCenter user, User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    VaccineCenter vac = new VaccineCenter();
    var wrapper = new Object(){
      Boolean loggedIn = true;
    };
    display.clearConsole();
    while (wrapper.loggedIn) {
      display.println("Welcome " + user.name, "success");
      display.println("Below is what you can do : ", "input");
      display.println("1. View all recipient data and vaccine status in table", "basic");
      display.println("2. Set appointment", "basic");
      display.println("3. Update recipient status based on doses received", "basic");
      display.println("4. Statistic", "basic");
      display.println("5. Logout", "basic");
      display.print("Enter you option : ", "input");
      
      try {
        int menu = Integer.parseInt(sc.nextLine()); 
        switch (menu) {
          case 1:
            vac.viewRecipient(user, users);
            break;
          case 2:
            users = vac.setAppointment(user, users);
            break;
          case 3:
            users = vac.updateStatus(user, users);
            break;
          case 5:
            wrapper.loggedIn = false;
          default:
            display.wrongMenu();
            break;
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }
    App.mainMenu(users);
  }

  public static void mainMenu(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    Boolean isWrong = true;
    Recipient recipient = new Recipient();
    while(isWrong){
      try {
        display.clearConsole();
        display.println("Welcome to MySejahtera", "success");
        display.println("Please select your menu below :", "input");
        display.println("1. Login to system", "basic");
        display.println("2. Register new user", "basic");
        display.println("3. Exit system", "basic");
        display.print("Enter the number : ", "input");
        int menu = Integer.parseInt(sc.nextLine());
        if (menu == 1){
          loginLoop(users);
        }else if(menu == 2){
          users = recipient.registerRecipient(users);
        }else if (menu == 3) {
          display.println("Exiting program...", "error");
          isWrong = false;
          writeFile(users);
          System.exit(0);
          break;
        }else{
          display.wrongMenu();
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }
  }

  public static void writeFile(User users){
    String filePath = "users.csv";
    try {
      FileWriter outputfile = new FileWriter(filePath);
      CSVWriter writer = new CSVWriter(outputfile);
      
      List<String[]> data = new ArrayList<String[]>();
      data.add(new String[] { 
        "name", "phone", "status/limit_dose", 
        "first_dose/total_dose", "second_dose/center_capacity",
        "email", "password", "type"
      });
      users.mohList.forEach(user->{
        data.add(new String[] { 
          user.name, user.phone,
          "", "", "",
          user.email, user.password, "moh"
        });
      });
      users.vacList.forEach(user->{
        data.add(new String[] { 
          user.name, user.phone,
          Integer.toString(user.limit_dose), Integer.toString(user.total_dose), Integer.toString(user.center_capacity),
          user.email, user.password, "vac"
        });
      });
      users.recipientsList.forEach(user->{
        String firstDoseString = "";
        String secondDoseString = "";
        if (user.first_dose.get("isEmpty").equals("false")){
          firstDoseString = user.first_dose.get("center") + "@" + user.first_dose.get("date") + "@" + user.first_dose.get("type") ;
        }
        if (user.second_dose.get("isEmpty").equals("false")){
          secondDoseString = user.second_dose.get("center") + "@" + user.second_dose.get("date") + "@" + user.second_dose.get("type") ;
        }
        data.add(new String[] { 
          user.name, user.phone,
          user.status,
          firstDoseString, secondDoseString,
          user.email, user.password, "recipient"
        });
      });
      writer.writeAll(data);

      // closing writer connection
      writer.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }


}