package com.mysejahtera;

import java.util.*;

public class MinHealth {
  String name, phone, email, password;
  public MinHealth(){};
  
  public MinHealth(String[] data){
    this.name = data[0];
    this.phone = data[1];
    this.email = data[5];
    this.password = data[6];
  }
  
  public void viewRecipient(User users){
    TerminalColor display = new TerminalColor();
    display.clearConsole();
    var wrapper = new Object(){
      int count = 1;
    };
    display.println("List recipient registered in the system", "success");
    String tableFormat = "| %-3s | %-40s | %-15s | %-15s |%n";
    System.out.format("+-----+------------------------------------------+-----------------+-----------------+%n");
    System.out.format("+ No. + Name                                     + 1st Dose Center + 2nd Dose Center +%n");
    System.out.format("+-----+------------------------------------------+-----------------+-----------------+%n");
    users.recipientsList.forEach(user ->{
      String first_center = user.first_dose.get("isEmpty").equals("false") ? user.first_dose.get("date") : "Not assign";
      String second_center = user.second_dose.get("isEmpty").equals("false") ? user.second_dose.get("date") : "Not assign";
      System.out.format(tableFormat, wrapper.count, user.name, first_center, second_center);
      wrapper.count++;
    });
    System.out.format("+-----+------------------------------------------+-----------------+-----------------+%n");
    display.enterKey();
  }

  /**
   * search user by looping through the users.recipientsList
   * @param users
   */
  public void searchRecipient(User users){
    // Scanner sc = new Scanner(System.in);
    // TerminalColor display = new TerminalColor();
    searchByName(users);
    // var wrapper = new Object(){
    //   Boolean isExit = false;
    // };
    // while(!wrapper.isExit){
    //   display.clearConsole();
    //   display.println("Welcome to search function of the system", "success");
    //   display.println("Here list what you could do here", "input");
    //   display.println("1. Search by name", "basic");
    //   // display.println("2. Search by vaccine center", "basic");
    //   display.println("3. Exit function", "basic");
    //   display.print("Enter your option : ", "input");
    //   int menu = sc.nextInt();
    //   switch (menu) {
    //     case 1:
    //       searchByName(users);
    //       break;
    //     case 2:
    //       searchByVaccineCenter(users);
    //       break;
    //     case 3:
    //       wrapper.isExit = true;
    //     default:
    //       display.wrongMenu();
    //       break;
    //   }
    // }
  }

  public void searchByName(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    display.println("Search by name is initiated....", "warning");
    display.print("Enter the name you want to search : ", "input");
    String name = sc.nextLine();
    var wrapper = new Object(){
      int count = 1;
      Boolean foundUser = false;
    };
    String leftAlignFormat2 = "| %-3s | %-40s | %-15s | %-15s |%n";
    System.out.format("+-----+------------+-----------------------------+-----------------+%n");
    System.out.format("+ No. + Name                                     + 1st Dose Center + 2nd Dose Center +%n");
    System.out.format("+-----+------------+-----------------------------+-----------------+%n");
    users.recipientsList.forEach(user->{
      if (user.name.toLowerCase().contains(name.toLowerCase())){
        wrapper.foundUser = true;
        String first_center = user.first_dose.get("isEmpty").equals("false") ? user.first_dose.get("date") : "Not assign";
        String second_center = user.second_dose.get("isEmpty").equals("false") ? user.second_dose.get("date") : "Not assign";
        System.out.format(leftAlignFormat2, wrapper.count, user.name, first_center, second_center);
        wrapper.count++;
      }
    });
    System.out.format("+-----+------------+-----------------------------+-----------------+%n");
    if (wrapper.foundUser == false){
      display.println("No recipient found", "error");
    }
    display.enterKey();
  }

  public void searchByVaccineCenter(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    display.println("Search by recipient by vaccine center is initiated....", "warning");
    display.print("Select vaccine center that you like to see the recipient : ", "input");
    var wrapper = new Object(){
      int count = 1;
      int recipientCounter = 0;
    };
    String leftAlignFormat = "| %-3s | %-10s | %-4d | %-4d | %-4d | %-4d |%n";
    System.out.format(leftAlignFormat, "No.","Name", "Limit Dose", "Total Vaccined", "Center Capacity", "Total Recipient");
    users.vacList.forEach(vac -> {
      wrapper.recipientCounter = 0;
      users.recipientsList.forEach(recipient->{
        if (recipient.first_dose.get("isEmpty").equals("false")){
          if (recipient.first_dose.get("center").equals(vac.name)){
            wrapper.recipientCounter++;
          }else if (recipient.second_dose.get("isEmpty").equals("false")){
            if (recipient.first_dose.get("center").equals(vac.name)){
              wrapper.recipientCounter++;
            }
          }
        }
        wrapper.recipientCounter++;
      });
      System.out.format(leftAlignFormat, wrapper.count, vac.name, vac.limit_dose, vac.total_dose, vac.center_capacity, wrapper.recipientCounter);
      wrapper.count++;
    });
    display.enterKey();
  }

  /**
   * increase vaccine limit to vaccine center and return users(contain updated data) to main menu
   * @param users
   * @return users
   */
  public User supplyVaccine(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    System.out.println(users.vacList.size());
    display.println("List of Vaccine Center", "warning");
    var helper = new Object(){
      int count = 1;
      Boolean isWrong = true;
      int name = 0;
      Boolean isWrongInp = true;
      int supply = 0;
    };
    String leftAlignFormat = "| %-3s | %-10s | %-10s | %-10s | %-15s |%n";
    System.out.format("+-----+------------+------------+------------+-----------------+%n");
    System.out.format("+ No. + Name       + Limit Dose + Total Vaccined + Center Capacity +%n");
    System.out.format("+-----+------------+------------+------------+-----------------+%n");
    users.vacList.forEach(user ->{
      System.out.format(leftAlignFormat, helper.count, user.name, user.limit_dose, user.total_dose, user.center_capacity);
      // display.println(helper.count + ". " + user.name + " Total Supply : " + user.limit_dose + " Total vaccined : " + user.total_dose, "basic");
      helper.count++;
    });
    System.out.format("+-----+------------+------------+------------+-----------------+%n");
    while(helper.isWrong){
      try {
        display.print("Enter the vaccine center to increase the supply [ex: 1]: ", "input");
        helper.name = Integer.parseInt(sc.nextLine());
        if (helper.name > users.vacList.size()){
          display.wrongMenu();
        }else{
          helper.isWrong = false;
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }
    VaccineCenter selectedVac = users.vacList.get(helper.name - 1);
    display.println(selectedVac.name + " has been selected with total supply of " + selectedVac.limit_dose, "basic");
    while (helper.isWrongInp){
      try {
        display.print("Enter amount to be supply [ex: 10] : ", "input");
        helper.supply = Integer.parseInt(sc.nextLine());
        helper.isWrongInp = false;
      } catch (Exception e) {
        display.wrongMenu();
      }
    }
    display.println("Computing value....", "warning");
    helper.supply = selectedVac.setSupply(helper.supply);
    display.println("Current total supply is " + helper.supply, "warning");
    // sending update data to main object which is users
    users.vacList.set(helper.name - 1 , selectedVac);
    display.enterKey();
    ;
    return users;
  }

  /**
   * assign recipient to vaccine center and return users(contain updated data) to main menu
   * @param userss
   * @return users
   */
  public User assignRecipient(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    var selectedObj = new Object(){
      Boolean isWrongRec = true;
      Boolean isWrongVac = true;
      Recipient recipient = new Recipient();
      VaccineCenter vac = new VaccineCenter();
      int indSel = 0;
      int indVac = 0;
      int countVac = 1;
      int count = 1;
      Boolean doseWrong = true;
      int dose = 0;
    };
    display.println("Set appointment for : ", "input");
    display.println("1. First Dose", "basic");
    display.println("2. Second Dose", "basic");
    while(selectedObj.doseWrong){
      try {
        display.print("Select which dose you want to set appointment : ", "input");
        selectedObj.dose = Integer.parseInt(sc.nextLine());
        if (selectedObj.dose > 2 || selectedObj.dose == 0){
          display.wrongMenu();          
        }else{
          selectedObj.doseWrong = false;
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }

    display.println("List of recipient", "warning");
    String leftAlignFormat = "| %-3s | %-40s | %-15s | %-13s |%n";
    if (selectedObj.dose == 1){
      System.out.format("+-----+------------------------------------------+-----------------+---------------+%n");
      System.out.format("+ No. + Name                                     + 1st Dose Center + 1st Dose Date +%n");
      System.out.format("+-----+------------------------------------------+-----------------+---------------+%n");
      users.recipientsList.forEach(user->{
        String center = user.first_dose.get("isEmpty").equals("true") ? "Not Assigned" : user.first_dose.get("center");
        String date = user.first_dose.get("isEmpty").equals("true") ? "Not Assigned" : user.first_dose.get("date");
        System.out.format(leftAlignFormat, selectedObj.count, user.name, center, date );
        selectedObj.count++;
      });
      System.out.format("+-----+------------------------------------------+-----------------+---------------+%n");
    }else{
      System.out.format("+-----+------------------------------------------+-----------------+---------------+%n");
      System.out.format("+ No. + Name                                     + 2nd Dose Center + 2nd Dose Date +%n");
      System.out.format("+-----+------------------------------------------+-----------------+---------------+%n");
      users.recipientsList.forEach(user->{
        String center = user.second_dose.get("isEmpty").equals("true") ? "Not Assigned" : user.first_dose.get("center");
        String date = user.second_dose.get("isEmpty").equals("true") ? "Not Assigned" : user.second_dose.get("date");
        System.out.format(leftAlignFormat, selectedObj.count, user.name, center, date );
        selectedObj.count++;
      });
      System.out.format("+-----+------------------------------------------+-----------------+---------------+%n");
    }
    
    while(selectedObj.isWrongRec){
      try {
        display.print("Enter the recipient number [ex: 1] : ", "input");
        selectedObj.indSel = Integer.parseInt(sc.nextLine());
        int recipientSize = users.recipientsList.size();
        if (selectedObj.indSel > recipientSize){
          display.wrongMenu();
        }else{
          selectedObj.recipient = users.recipientsList.get(selectedObj.indSel - 1);
          display.println("You have select " + selectedObj.recipient.name, "basic");
          selectedObj.isWrongRec = false;
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }
    
    String leftAlignFormat2 = "| %-3s | %-10s | %-18s |%n";
    System.out.format("+-----+------------+--------------------+%n");
    System.out.format("+ No. + Name       + Remaining Capacity +%n");
    System.out.format("+-----+------------+--------------------+%n");
    users.vacList.forEach(vac->{
      System.out.format(leftAlignFormat2, selectedObj.countVac, vac.name, (vac.center_capacity - vac.total_dose));
      selectedObj.countVac++;
    });
    System.out.format("+-----+------------+--------------------+%n");

    while(selectedObj.isWrongVac){
      try {
        display.print("Enter the Vaccine Center number [ex: 1] : ", "input");
        selectedObj.indVac = Integer.parseInt(sc.nextLine());
        if (selectedObj.indVac > selectedObj.countVac - 1){
          display.wrongMenu();
        }else{
          selectedObj.vac = users.vacList.get(selectedObj.indVac - 1);
          display.println("You have select " + selectedObj.vac.name, "basic");
          display.println("Assign " + selectedObj.recipient.name + " to " + selectedObj.vac.name, "basic");
          // assign recipient to vaccine center
          if (selectedObj.dose == 1){
            selectedObj.recipient.first_dose.put("center", selectedObj.vac.name);
            selectedObj.recipient.first_dose.put("date", "Not Assign");
            selectedObj.recipient.first_dose.put("type", "Not Assign");
            selectedObj.recipient.first_dose.put("isEmpty", "false");
          }else{
            selectedObj.recipient.second_dose.put("center", selectedObj.vac.name);
            selectedObj.recipient.second_dose.put("date", "Not Assign");
            selectedObj.recipient.second_dose.put("type", "Not Assign");
            selectedObj.recipient.second_dose.put("isEmpty", "false");
          }
          // update recipient data in array list
          users.recipientsList.set(selectedObj.indSel - 1 , selectedObj.recipient);
          selectedObj.isWrongVac = false;
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }
    
    // update capacity value on vaccine center
    selectedObj.vac.updateTotalDose();
    users.vacList.set(selectedObj.indVac - 1, selectedObj.vac);
    display.enterKey();
    return users;
  }

  public void analytics(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    var wrapper = new Object(){
      int count = 1;
      int recipientCounter = 0;
    };
    String leftAlignFormat = "| %-3s | %-10s | %-4d | %-4d | %-4d | %-4d |%n";
    System.out.format(leftAlignFormat, "No.","Name", "Limit Dose", "Total Vaccined", "Center Capacity", "Total Recipient");
    users.vacList.forEach(vac -> {
      wrapper.recipientCounter = 0;
      users.recipientsList.forEach(recipient->{
        if (recipient.first_dose.get("isEmpty").equals("false")){
          if (recipient.first_dose.get("center").equals(vac.name)){
            wrapper.recipientCounter++;
          }else if (recipient.second_dose.get("isEmpty").equals("false")){
            if (recipient.first_dose.get("center").equals(vac.name)){
              wrapper.recipientCounter++;
            }
          }
        }
        wrapper.recipientCounter++;
      });
      System.out.format(leftAlignFormat, wrapper.count, vac.name, vac.limit_dose, vac.total_dose, vac.center_capacity, wrapper.recipientCounter);
      wrapper.count++;
    });
    display.enterKey();;
  }
  
  public Map<String, String> getCredential(){
    Map<String, String> map= new HashMap<String, String>();
    map.put("email", this.email);
    map.put("password", this.password);
    return map;
  } 
}