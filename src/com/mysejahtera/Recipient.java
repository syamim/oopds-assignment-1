package com.mysejahtera;

import java.util.*;


/**
 * Recipient class
 * Hold information about recipient and function
 */
public class Recipient {
  String name,phone,status,email,password;
  Map<String, String> first_dose,second_dose;

  public Recipient(){};

  public Recipient (String[] data){
    this.name = data[0];
    this.phone = data[1];
    this.status = data[2];
    setDoseMap(data[3], "first");
    setDoseMap(data[4], "second");
    this.email = data[5];
    this.password = data[6];
  }
  
  /**
   * Generate hashmap of doses receive from csv file
   * @param doseData string of dose detail with '@' as the divider
   * @param dose string of dose type
   */
  public void setDoseMap(String doseData, String dose){
    Map<String,String> map = new HashMap<String,String>();
    if (!doseData.equals("")){
      String[] dataArr = doseData.split("@",3);
      map.put("center", dataArr[0]);
      map.put("date", dataArr[1]);
      map.put("type", dataArr[2]);
      map.put("isEmpty", "false");
    }else{
      map.put("isEmpty", "true");
    }
    if (dose == "first") {
      this.first_dose = map;
    } else {
      this.second_dose = map;
    }
  }

  // getters
  public String getName(){
    return this.name;
  }
  public String getPhone(){
    return this.phone;
  }
  public String getStatus(){
    return this.status;
  }
  public Map<String, String> getFirstDose(){
    return this.first_dose;
  }
  public Map<String, String> getSecondDose(){
    return this.second_dose;
  }

  /**
   * used for logi
   * @return hashmap of credential with key of email and password
   */
  public Map<String, String> getCredential(){
    Map<String, String> map= new HashMap<String, String>();
    map.put("email", this.email);
    map.put("password", this.password);
    return map;
  }

  /**
   * Update status to new status
   * @param status
   */
  public void updateStatus(String status){
    this.status = status;
  }

  /**
   * Create new recipient object and insert to list and return all users to main
   * @param users
   * @return
   */
  User registerRecipient(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    String name,phone,status,email,password;
    display.println("Register now to get you vaccinated", "success");
    display.print("Enter your name : ", "input");
    name = sc.nextLine();
    display.print("Enter your phone number : ", "input");
    phone = sc.nextLine();
    display.print("Enter your email/username : ", "input");
    email = sc.nextLine();
    display.print("Enter your password : ", "input");
    password = sc.nextLine();
    String[] recipientData = {name, phone, "Pending", "", "", email, password, "recipient"};
    Recipient tempRecipient = new Recipient(recipientData);
    users.recipientsList.add(tempRecipient);
    display.println("⚙ Creating your account...", "warning");
    display.println("✔ Your account successfully created ", "success");
    return users;
  }
}
