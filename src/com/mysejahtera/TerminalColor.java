package com.mysejahtera;
import java.util.*;


public class TerminalColor{
  String BLACK = "\033[0;30m";
  String RED = "\033[0;31m";    
  String GREEN = "\033[0;32m";  
  String YELLOW = "\033[0;33m"; 
  String BLUE = "\033[0;34m";   
  String MAGENTA = "\033[0;35m";
  String CYAN = "\033[0;36m";
  String WHITE = "\033[0;37m";
  String RESET = "\033[0m";
  /**
   * Print receive text with color + new line
   * @param text
   * @param type
   */
  public void println(String text, String type){
    if (type.equals("warning")){
      text = "⚠" + this.YELLOW + text;
    }else if(type.equals("error")){
      text = "🛑" + this.RED + text;
    }else if(type.equals("input")){
      text = "✏" + this.BLUE + text;
    }else if (type.equals("success")){
      text = this.GREEN + text;
    }
    text = text + this.RESET;
    System.out.println(text);
  }
  
  /**
   * Print receive text with color - new line
   * @param text
   * @param type
   */
  public void print(String text, String type){
    if (type.equals("warning")){
      text = "⚠" + this.YELLOW + text;
    }else if(type.equals("error")){
      text = "🛑" + this.RED + text;
    }else if(type.equals("input")){
      text = "✏" + this.BLUE + text;
    }else if (type.equals("success")){
      text = this.GREEN + text;
    }
    text = text + this.RESET;
    System.out.print(text);
  }

  /**
   * Clear console
   * CANNOT BE USED IN SUBMISSION
   */
  public void clearConsole(){
    // System.out.print("\033[H\033[2J");
    // System.out.flush();
  }

  public void wrongMenu(){
    System.out.println(this.RED + "You've select wrong option selected!" + this.RESET);
    System.out.println(this.YELLOW + "Please read the menu option carefully!" + this.RESET);
  }

  public void enterKey(){
    System.out.print(this.GREEN + "Press Enter to continue...." + this.RESET);
    Scanner sc = new Scanner(System.in);
    String temp = sc.nextLine();
    clearConsole();
  }
}