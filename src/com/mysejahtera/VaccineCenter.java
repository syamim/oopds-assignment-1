package com.mysejahtera;

import java.util.*;

/**
 * Class for holding all vaccine center data and method
 */
public class VaccineCenter {
  String name, phone, email, password;
  int limit_dose, total_dose, center_capacity;

  public VaccineCenter(){};

  public VaccineCenter(String[] data){    
    this.name = data[0];
    this.phone = data[1];
    this.limit_dose = Integer.parseInt(data[2]);
    this.total_dose = Integer.parseInt(data[3]);
    this.center_capacity = Integer.parseInt(data[4]);
    this.email = data[5];
    this.password = data[6];
  }

  /**
   * Print all recipient data based on the current logged in user
   * @param current_user 
   * @param users
   */
  public void viewRecipient(VaccineCenter current_user, User users){
    TerminalColor display = new TerminalColor();
    display.clearConsole();
    display.println("List of recipient at you vaccine center", "warning");

    String tableFormat = "| %-3s | %-40s | %-21s |%n";
    System.out.format("+-----+------------------------------------------+-----------------------+%n");
    System.out.format("+ No. + Name                                     + First Dose Date       +%n");
    System.out.format("+-----+------------------------------------------+-----------------------+%n");
    var wrapper = new Object(){
      int count = 1;
      int countSec = 1;
    };
    
    users.recipientsList.forEach(user -> {
      if (user.first_dose.get("isEmpty").equals("false")){
        if (user.first_dose.get("center").equals(current_user.name)){
          System.out.format(tableFormat, wrapper.count, user.name, user.first_dose.get("date"));
          wrapper.count++;
        }
      }
    });
    System.out.format("+-----+------------------------------------------+-----------------------+%n");
    System.out.println("");

    String tableFormat2 = "| %-3s | %-40s | %-21s |%n";
    System.out.format("+-----+------------------------------------------+-----------------------+%n");
    System.out.format("+ No. + Name                                     + Second Dose Date      +%n");
    System.out.format("+-----+------------------------------------------+-----------------------+%n");
    users.recipientsList.forEach(user -> {
      if (user.second_dose.get("isEmpty").equals("false")){
        if (user.second_dose.get("center").equals(current_user.name)){
          System.out.format(tableFormat2, wrapper.countSec, user.name, user.second_dose.get("date"));
          wrapper.countSec++;
        }
      }
    });
    System.out.format("+-----+------------------------------------------+-----------------------+%n");

    display.enterKey();
  }

  /**
   * Search recipient based on the current user
   * @param current_user
   * @param users
   */
  public void searchRecipient(VaccineCenter current_user, User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    display.println("Search by name is initiated....", "warning");
    display.print("Enter the name you want to search : ", "input");
    String name = sc.nextLine();
    var wrapper = new Object(){
      int count = 1;
      Boolean foundUser = false;
    };
    
    String tableFormat = "| %-3s | %-40s | %-20s |%n";
    System.out.format("+-----+------------------------------------------+----------------------+%n");
    System.out.format("+ No. + Name                                     + Status               +%n");
    System.out.format("+-----+------------------------------------------+----------------------+%n");
    users.recipientsList.forEach(user->{
      if (user.name.toLowerCase().contains(name.toLowerCase())){
        if (!user.first_dose.get("isEmpty").equals("false")){
          if (user.first_dose.get("center").equals(current_user.name)){
            wrapper.foundUser = true;
            System.out.format(tableFormat, wrapper.count, user.name, user.status);
            wrapper.count++;
          }
        }
      }
    });
    System.out.format("+-----+------------------------------------------+----------------------+%n");

    if (wrapper.foundUser == false){
      display.println("No recipient found", "error");
    }
    display.enterKey();
  }

  /** Update limit dose when MOH update */
  public int setSupply(int amount){
    this.limit_dose += amount;
    return this.limit_dose;
  }
  
  /**
   * update total dose when updating user status
   */
  public void updateTotalDose(){
    this.total_dose += 1;
  }
  
  /**
   * Set appointment for recipient
   * @param current_user
   * @param users
   * @return users to receive in main menu
   */
  public User setAppointment(VaccineCenter current_user, User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    var helper = new Object(){
      int dose = 0;
      Boolean doseWrong = true;
      int countRep = 1;
      int indRec = 0;
      Boolean wrongInd = true;
      Boolean foundUser = false;
      List<Recipient> recipients = new ArrayList<>();
    };
    display.println("Set appointment for : ", "input");
    display.println("1. First Dose", "basic");
    display.println("2. Second Dose", "basic");
    while(helper.doseWrong){
      try {
        display.print("Select which dose you want to set appointment : ", "input");
        helper.dose = Integer.parseInt(sc.nextLine());
        if (helper.dose > 2 || helper.dose == 0){
          display.wrongMenu();          
        }else{
          helper.doseWrong = false;
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }

    display.println("List of recipient assigned to you without appointment yet", "warning");
    if (helper.dose == 1){
      String tableFormat = "| %-3s | %-40s | %-20s |%n";
      System.out.format("+-----+------------------------------------------+----------------------+%n");
      System.out.format("+ No. + Name                                     + Status               +%n");
      System.out.format("+-----+------------------------------------------+----------------------+%n");
      users.recipientsList.forEach(recipient ->{
        if (recipient.first_dose.get("isEmpty").equals("false")){
          if (recipient.first_dose.get("center").equals(current_user.name)){
            if (recipient.first_dose.get("date").equals("Not Assign")){
              helper.foundUser = true;
              System.out.format(tableFormat, helper.countRep, recipient.name, recipient.status);
              helper.recipients.add(recipient);
              helper.countRep++;
            }
          }
        }
      });
      System.out.format("+-----+------------------------------------------+----------------------+%n");
      if (helper.foundUser){
        while(helper.wrongInd){
          try {
            display.print("Enter the recipient number [ex: 1] : ", "input");
            helper.indRec = Integer.parseInt(sc.nextLine());
            if (helper.indRec > helper.countRep - 1){
              display.wrongMenu();
            }else{
              helper.wrongInd = false;
            }
          } catch (Exception e) {
            display.wrongMenu();
          }
        }
        Recipient selectedRecipient = helper.recipients.get(helper.indRec - 1);
        display.println("You have select " + selectedRecipient.name, "basic");
        display.print("Enter date for the appointment of the recipient [ex: 31/12/2000] : ", "input");
        String appointmentDate = sc.nextLine();
        display.print("Enter time for the appointment of the recipient [ex: 14:00:00 ] : ", "input");
        String appointmentTime = sc.nextLine();
        String gabung = appointmentDate + " " + appointmentTime; 
        selectedRecipient.first_dose.put("date", gabung);
        display.println("Appointment for " + selectedRecipient.name + " has been set at " + appointmentTime + " on " + appointmentDate, "basic");
        users.recipientsList.set(helper.indRec - 1, selectedRecipient);
      }else{
        display.println("No recipient found", "error");
      }
    }else{
      String tableFormat = "| %-3s | %-10s | %-20s |%n";
      System.out.format("+-----+------------+----------------------+%n");
      System.out.format("+ No. + Name       + Status               +%n");
      System.out.format("+-----+------------+----------------------+%n");
      users.recipientsList.forEach(recipient ->{
        if (recipient.second_dose.get("isEmpty").equals("false")){
          if (recipient.second_dose.get("center").equals(current_user.name)){
            if (recipient.second_dose.get("date").equals("Not Assign")){
              helper.foundUser = true;
              System.out.format(tableFormat, helper.countRep, recipient.name, recipient.status);
              helper.recipients.add(recipient);
              helper.countRep++;
            }
          }
        }
      });
      System.out.format("+-----+------------+----------------------+%n");
      if (helper.foundUser){
        while(helper.wrongInd){
          try {
            display.print("Enter the recipient number [ex: 1] : ", "input");
            helper.indRec = Integer.parseInt(sc.nextLine());
            if (helper.indRec > helper.countRep - 1){
              display.wrongMenu();
            }else{
              helper.wrongInd = false;
            }
          } catch (Exception e) {
            display.wrongMenu();
          }
        }
        Recipient selectedRecipient = helper.recipients.get(helper.indRec - 1);
        display.println("You have select " + selectedRecipient.name, "basic");
        display.print("Enter date for the appointment of the recipient [ex: 31/12/2000] : ", "input");
        sc.nextLine(); // consume previous line
        String appointmentDate = sc.nextLine();
        display.print("Enter time for the appointment of the recipient [ex: 14:00:00 ] : ", "input");
        String appointmentTime = sc.nextLine();
        String gabung = appointmentDate + " " + appointmentTime; 
        selectedRecipient.second_dose.put("date", gabung);
        display.println("Appointment for " + selectedRecipient.name + " has been set at " + appointmentTime + " on " + appointmentDate, "basic");
        users.recipientsList.set(helper.indRec - 1, selectedRecipient);
      }else{
        display.println("No recipient found", "error");
      }
    }
    display.enterKey();
    return users;
  }
  
  public User updateStatus(VaccineCenter current_user, User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    display.println("Update recipient status", "warning");
    display.println("List of recipient in you center", "warning");
    var helper = new Object(){
      int count = 1;
      int countOption = 1;
      Boolean isWrong = true;
      int indRec = 0;
      Boolean wrongOpt = true;
      int indOpt = 0;
      Boolean foundUser = false;
      List<Recipient> recipients = new ArrayList<>();
    };
    
    String tableFormat = "| %-3s | %-40s | %-20s |%n";
    System.out.format("+-----+------------------------------------------+----------------------+%n");
    System.out.format("+ No. + Name                                     + Status               +%n");
    System.out.format("+-----+------------------------------------------+----------------------+%n");
    users.recipientsList.forEach(recipient->{
      if (recipient.first_dose.get("isEmpty").equals("false")){
        if (recipient.first_dose.get("center").equals(current_user.name)){
          helper.foundUser = true;
          System.out.format(tableFormat, helper.count, recipient.name, recipient.status);
          helper.recipients.add(recipient);
          helper.count++;
        }
      }
    });
    System.out.format("+-----+------------------------------------------+----------------------+%n");
    if (helper.foundUser){
      while(helper.isWrong){
        try {
          display.print("Enter the recipient number you want to select [ex: 1]: ", "input");
          helper.indRec = Integer.parseInt(sc.nextLine());
          if (helper.indRec > helper.count - 1){
            display.wrongMenu();
          }else{
            helper.isWrong = false;
          }
        } catch (Exception e) {
          display.wrongMenu();
        }
      }
      Recipient selectedRecipient = helper.recipients.get(helper.indRec - 1);
      display.println("You have select " + selectedRecipient.name, "basic");
      display.println("Status Options", "warning");
      String[] options = {"Pending", "1st Dose Appointment",  "1st Dose Completed",  "2nd Dose Appointment",  "2nd Dose Completed"};
      for (String option : options){
        display.println(helper.countOption + ". " + option, "basic");
        helper.countOption++;
      }
      while (helper.wrongOpt){
        try {
          display.print("Enter the option to update with [ex: 1]: ", "input");
          helper.indOpt = Integer.parseInt(sc.nextLine());
          if (helper.indOpt > 5 || helper.indOpt == 0){
            display.wrongMenu();
          }else{
            helper.wrongOpt = false;
          }
        } catch (Exception e) {
          display.wrongMenu();
        }
      }
  
      selectedRecipient.updateStatus(options[helper.indOpt -1]);
      display.println("Update status for " + selectedRecipient.name + " to " + options[helper.indOpt -1], "basic");
      users.recipientsList.set(helper.indRec - 1, selectedRecipient);
    }else{
      display.println("No recipient foun", "error");;
    }
    display.enterKey();
    return users;
  }
  
  public void analytics(VaccineCenter current_user, User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    var helper = new Object(){
      int count = 0;
    };
    users.recipientsList.forEach(recipient->{
      if (recipient.status.equals("Pending")){
        helper.count++;
      }
    });
    display.println("Total Unvaccinated Recipient : " + current_user.total_dose, "basic");
    display.println("Total Vaccinated Recipient : " + current_user.total_dose, "basic");
    display.enterKey();
  } 

  public Map<String, String> getCredential(){
    Map<String, String> map= new HashMap<String, String>();
    map.put("email", this.email);
    map.put("password", this.password);
    return map;
  }
}